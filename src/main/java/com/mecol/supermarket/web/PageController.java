package com.mecol.supermarket.web;

import com.mecol.supermarket.entity.User;
import com.mecol.supermarket.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

@Controller
public class PageController {

    @Autowired
    private UserService userService;


    //输入http://localhost:8080/ssm_supermarket_mysql/即可打开登录界面

    @RequestMapping("/login")
    public String login(HttpSession session){
        session.removeAttribute("userOnLogin");
        return "login";
    }

    /*
     * 查询登录账号密码是否正确
     */
    @RequestMapping(value="/dologin" ,method= RequestMethod.POST)
    public String dologin(HttpSession session, @RequestParam String userCode, @RequestParam String password){
        User u=userService.login(userCode, password);
        if(u!=null){
            session.setAttribute("userOnLogin", u);
            session.removeAttribute("error");
            return "redirect:/sys/main";
        }else{
            session.setAttribute("error", "用户名或密码错误");
            session.removeAttribute("successSavePwd");
            return "redirect:/login";
        }
    }

    /*
     * 主界面
     */
    @RequestMapping("/sys/main")
    public String main(){
        return "frame";
    }
    /*
     * 退出系统
     */
    @RequestMapping("/sys/logout")
    public String logout(HttpSession session){
        session.removeAttribute("userOnLogin");
        session.removeAttribute("successSavePwd");
        return "redirect:/login";
    }

}
