package com.mecol.supermarket.service;

import com.mecol.supermarket.dao.ProviderDao;
import com.mecol.supermarket.entity.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProviderServiceImpl implements ProviderService{

    @Autowired
    private ProviderDao providerDao;
    @Override
    public List<Provider> findAllProvider() {
        return providerDao.findAllProvider();
    }

    @Override
    public List<Provider> findByCodeOrName(String proCode, String proName) {
        return providerDao.findByCodeOrName(proCode,proName);
    }

    @Override
    public Provider findProvider(int id) {
        return providerDao.findProvider(id);
    }

    @Override
    public boolean proModify(Provider p) {
        if(providerDao.proModify(p)>0){
            return true;
        }
        return false;
    }

    @Override
    public String proDelete(int id) {
        //删除之前先判断 此供货商下有没有订单 如果有就删除失败

        if(providerDao.findBill(id)==0){
            if(providerDao.proDelete(id)>0){
                return "true";
            }else{
                return "false";
            }
        }
        else{
            return String.valueOf(providerDao.findBill(id));
        }
    }

    @Override
    public boolean proAdd(Provider p) {
        if(providerDao.proAdd(p)>0){
            return true;
        }
        return false;
    }
}
