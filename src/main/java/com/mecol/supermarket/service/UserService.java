package com.mecol.supermarket.service;

import com.mecol.supermarket.entity.Page;
import com.mecol.supermarket.entity.Role;
import com.mecol.supermarket.entity.User;

import java.util.List;

public interface UserService {
    User login(String userCode, String password);

    List<User> findUserByCount(String queryname, Integer userRole, Page page);

    List<Role> findAllRole();

    User findUserById(int id);

    boolean userModify(User u);

    String userDel(int id);

    boolean userAdd(User u);

    String findCode(String userCode);

    boolean pwdModify(Integer id, String newpassword);
}
