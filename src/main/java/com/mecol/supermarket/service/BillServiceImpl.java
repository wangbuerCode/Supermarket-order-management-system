package com.mecol.supermarket.service;

import com.mecol.supermarket.dao.BillDao;
import com.mecol.supermarket.entity.Bill;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BillServiceImpl implements BillService{

    @Autowired
    private BillDao billDao;

    @Override
    public List<Bill> findByNameIdAndIsPayment(String productName, Integer providerId, Integer isPayment) {
        return billDao.findByNameIdAndIsPayment(productName,providerId,isPayment);
    }

    @Override
    public Bill findBillById(int id) {
        return billDao.findBillById(id);
    }

    @Override
    public boolean billModify(Bill bill) {
        return billDao.billModify(bill);
    }

    @Override
    public String billDel(int id) {
        /*
        return billDao.billDel(id);
        */
        if(billDao.findBillById(id)!=null){
            if(billDao.billDel(id)>0){
                return "true";
            }else{
                return "false";
            }
        }
        return "notexist";
    }

    @Override
    public boolean billAdd(Bill bill) {
        // TODO Auto-generated method stub
        if(billDao.billAdd(bill)>0){
            return true;
        }
        return false;
    }
}
