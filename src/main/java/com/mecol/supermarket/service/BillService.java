package com.mecol.supermarket.service;

import com.mecol.supermarket.entity.Bill;

import java.util.List;

public interface BillService {
    List<Bill> findByNameIdAndIsPayment(String productName, Integer providerId, Integer isPayment);

    Bill findBillById(int id);

    boolean billModify(Bill bill);

    String billDel(int id);

    boolean billAdd(Bill bill);
}
