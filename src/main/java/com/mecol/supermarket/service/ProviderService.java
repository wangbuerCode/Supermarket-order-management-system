package com.mecol.supermarket.service;

import com.mecol.supermarket.entity.Provider;

import java.util.List;

public interface ProviderService {
    List<Provider> findAllProvider();

    List<Provider> findByCodeOrName(String proCode, String proName);

    Provider findProvider(int id);

    boolean proModify(Provider p);

    String proDelete(int id);

    boolean proAdd(Provider p);
}
