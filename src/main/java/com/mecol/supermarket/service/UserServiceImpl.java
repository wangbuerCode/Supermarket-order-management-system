package com.mecol.supermarket.service;

import com.mecol.supermarket.dao.UserDao;
import com.mecol.supermarket.entity.Page;
import com.mecol.supermarket.entity.Role;
import com.mecol.supermarket.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserDao userDao;
    @Override
    public User login(String userCode, String password) {
        return userDao.login(userCode,password);
    }

    @Override
    public List<User> findUserByCount(String username, Integer userRole, Page page) {
        page.setSize(5);
        page.setTotalCount(userDao.findUserCount(username,userRole));
        // TODO Auto-generated method stub
        return userDao.findUserByCount(username, userRole, (page.getCurrentpage()-1)*page.getSize(), page.getSize());
    }

    @Override
    public List<Role> findAllRole() {
        return userDao.findAllRole();
    }

    @Override
    public User findUserById(int id) {
        return userDao.findUserById(id);
    }

    @Override
    public boolean userModify(User u) {
        if(userDao.userModify(u)>0){
            return true;
        }
        return false;
    }

    @Override
    public String userDel(int id) {
        // TODO Auto-generated method stub
        if(userDao.findUserById(id)!=null){
            if(userDao.userDel(id)>0){
                return "true";
            }
            return "false";
        }
        return "notexist";
    }

    @Override
    public boolean userAdd(User u) {
        // TODO Auto-generated method stub
        if(userDao.userAdd(u)>0){
            return true;
        }
        return false;
    }

    @Override
    public String findCode(String userCode) {
        // TODO Auto-generated method stub
        if(userDao.findCode(userCode)!=null){
            return "exist";
        }
        return "noexist";
    }

    @Override
    public boolean pwdModify(Integer id, String newpassword) {
        // TODO Auto-generated method stub
        if(userDao.pwdModify(id, newpassword)>0){
            return true;
        }
        return false;
    }
}
