package com.mecol.supermarket.dao;

import com.mecol.supermarket.entity.Bill;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BillDao {

    List<Bill> findByNameIdAndIsPayment(@Param("productName") String productName,
                                        @Param("providerId") Integer providerId,
                                        @Param("isPayment") Integer isPayment);

    Bill findBillById(int id);

    boolean billModify(Bill bill);


    int billDel(int id);

    int billAdd(Bill bill);
}
