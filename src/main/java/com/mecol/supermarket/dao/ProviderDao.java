package com.mecol.supermarket.dao;

import com.mecol.supermarket.entity.Provider;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProviderDao {
    List<Provider> findAllProvider();

    List<Provider> findByCodeOrName(@Param("proCode") String proCode, @Param("proName") String proName);

    Provider findProvider(int id);

    int proModify(Provider p);

    int findBill(int id);

    int proDelete(int id);
    int proAdd(Provider provider);
}
