package com.mecol.supermarket.dao;

import com.mecol.supermarket.entity.Role;
import com.mecol.supermarket.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserDao {

    User login(@Param("userCode") String userCode, @Param("password") String password);

    int findUserCount(@Param("username") String username, @Param("userRole") Integer userRole);

    List<User> findUserByCount(@Param("username") String username, @Param("userRole") Integer userRole,
                               @Param("currentPage") int currentPage, @Param("size") int size);

    List<Role> findAllRole();

    User findUserById(int id);

    int userModify(User u);

    int userDel(int id);

    int userAdd(User u);

    Object findCode(String userCode);

    int pwdModify(@Param("id") Integer id, @Param("newpassword") String newpassword);
}
